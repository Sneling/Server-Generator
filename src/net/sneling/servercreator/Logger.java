package net.sneling.servercreator;

/**
 * Created by Sneling on 9/3/2016 for ServerCreator.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Logger {

    public static void print(Object msg){
        System.out.println(msg);
    }

}

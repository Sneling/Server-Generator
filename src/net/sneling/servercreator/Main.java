package net.sneling.servercreator;

import java.io.File;
import java.io.IOException;

public class Main {

    /*
    0: server version
     */
    public static void main(String[] args) {
        if(args.length < 1){
            Logger.print("Invalid parameters! Please specify a server version!");
            System.exit(1);
            return;
        }

        String serverVersion = args[0];

        if(getServerFile(serverVersion) == null || serverVersion.equalsIgnoreCase("BuildTools")){
            Logger.print("Invalid server version! Available servers : ");
            for(File file: FileUtil.findAvailableServers())
                Logger.print(" + " + file.getName().replaceAll(".jar", ""));

            System.exit(1);
            return;
        }

        try {
            ServerCreator.createServer(getServerFile(serverVersion));

            System.exit(0);
        } catch (IOException e) {
            Logger.print("An error occured:");
            e.printStackTrace();

            System.exit(1);
        }
    }

    private static File getServerFile(String ver){
        for(File file: FileUtil.findAvailableServers())
            if(file.getName().replaceAll(".jar", "").equalsIgnoreCase(ver))
                return file;

        return null;
    }

}

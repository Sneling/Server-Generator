package net.sneling.servercreator;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sneling on 9/10/2016 for ServerCreator.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ServerCreator {

    static void createServer(File server) throws IOException {
        String serverName = server.getName().replaceAll(".jar", "");

        File location = new File("servers/" + serverName);
        if(location.exists() ){
            Logger.print("ERROR! A server already exists on here! Delete the folder before continuing!");
            System.exit(1);
            return;
        }

        location.mkdirs();
        Logger.print("Created Folder at " + location.getPath());

        FileUtil.copy(server, new File(location.getPath() + "/spigot.jar"));
        Logger.print("Copied " + server.getName() + " to " + location.getName());

        for(File def: FileUtil.findDefaultFiles()){
            FileUtil.copy(def, new File(location.getPath() + "/" + def.getName()));
            Logger.print("Copied " + def.getName() + " to " + location.getPath());
        }
    }

}

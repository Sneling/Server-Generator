package net.sneling.servercreator;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by Sneling on 9/3/2016 for ServerCreator.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class FileUtil {

    static File[] findDefaultFiles(){
        return new File("s-files/default").listFiles();
    }

    static File[] findAvailableServers(){
        return new File("s-files/spigot").listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return !pathname.getName().contains("BuildTools");
            }
        });
    }

    static void copy(File from, File to) throws IOException {
        Files.copy(from.toPath(), to.toPath());
        if(to.getName().contains(".sh"))
            to.setExecutable(true);
    }

}
